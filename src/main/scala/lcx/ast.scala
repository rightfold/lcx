package lcx

import lcx.symbols._
import lcx.types.Variance

object ast {
  case class Module(definitions: List[Definition])

  sealed abstract class Definition
  case class TypeDefinition(name: String, kind: KindExpression, `type`: TypeExpression) extends Definition
  case class ValueDefinition(name: String, `type`: TypeExpression, value: ValueExpression) extends Definition

  sealed abstract class KindExpression
  case class NameKindExpression(name: String, var symbol: KindSymbol) extends KindExpression
  case class FunctionKindExpression(from: List[(Variance, KindExpression)], to: KindExpression) extends KindExpression

  sealed abstract class TypeExpression
  case class NameTypeExpression(name: String, var symbol: TypeSymbol) extends TypeExpression
  case class ApplicationTypeExpression(constructor: TypeExpression, arguments: List[TypeExpression]) extends TypeExpression

  sealed abstract class ValueExpression
  case class NameValueExpression(name: String, var symbol: ValueSymbol) extends ValueExpression
  case class TypeCallValueExpression(callee: ValueExpression, arguments: List[TypeExpression]) extends ValueExpression
  case class ValueCallValueExpression(callee: ValueExpression, arguments: List[ValueExpression]) extends ValueExpression
}
