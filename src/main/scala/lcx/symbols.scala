package lcx

import lcx.types._

object symbols {
  class KindSymbol(val kind: Kind)

  class TypeSymbol(val kind: Kind, var `type`: Type)

  class ValueSymbol(val `type`: Type)
}
