package lcx.check

import lcx.ast._
import lcx.check.error.TypeMismatchError
import lcx.check.scope.Scope
import lcx.check.types.{checkTypeExpression, typeExpressionType}
import lcx.symbols.ValueSymbol
import lcx.types.{subtype, Type, SingletonType}

object values {
  /**
    * Verifies a value expression and annotates it with symbols.
    */
  def checkValueExpression(scope: Scope, expression: ValueExpression): Unit =
    expression match {
      case nve @ NameValueExpression(name, _) =>
        nve.symbol = scope.getValueSymbol(name)
      case ValueCallValueExpression(callee, arguments) =>
        checkValueExpression(scope, callee)
        arguments.foreach(checkValueExpression(scope, _))
        ???
    }

  /**
    * Returns for the '''checked''' value expression what type it has.
    */
  def valueExpressionType(expression: ValueExpression): Type =
    expression match {
      case NameValueExpression(_, symbol) => SingletonType(symbol)
    }

  /**
    * Adds a value symbol to the scope for the given value definition.
    */
  def collectValueNameAndType(scope: Scope, definition: ValueDefinition): Unit = {
    val ValueDefinition(name, typeE, _) = definition
    checkTypeExpression(scope, typeE)
    val symbol = new ValueSymbol(typeExpressionType(typeE))
    scope.addValueSymbol(name, symbol)
  }

  /**
    * Verifies a value definition and annotates the value expression of it with
    * symbols.
    */
  def checkValueDefinition(scope: Scope, definition: ValueDefinition): Unit = {
    val ValueDefinition(name, typeE, valueE) = definition
    val symbol = scope.getValueSymbol(name)
    checkValueExpression(scope, valueE)
    val `type` = valueExpressionType(valueE)
    if (!subtype(`type`, symbol.`type`)) {
      throw TypeMismatchError(symbol.`type`, `type`)
    }
  }
}
