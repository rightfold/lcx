package lcx.check

import lcx.ast._
import lcx.check.error.KindMismatchError
import lcx.check.kinds.{checkKindExpression, kindExpressionKind}
import lcx.check.scope.Scope
import lcx.symbols.TypeSymbol
import lcx.types._

object types {
  /**
    * Verifies a type expression and annotates it with symbols.
    */
  def checkTypeExpression(scope: Scope, expression: TypeExpression): Unit =
    expression match {
      case nte @ NameTypeExpression(name, _) =>
        nte.symbol = scope.getTypeSymbol(name)
      case ApplicationTypeExpression(constructor, arguments) =>
        checkTypeExpression(scope, constructor)
        arguments.foreach(checkTypeExpression(scope, _))
        val constructorKind = typeKind(typeExpressionType(constructor))
        def mismatch = KindMismatchError(constructorKind, null /* TODO */)
        constructorKind match {
          case FunctionKind(from, _) =>
            if (from.map(_._2) != arguments.map(typeKind _ compose typeExpressionType)) {
              throw mismatch
            }
          case k => throw mismatch
        }
    }

  /**
    * Returns for the '''checked''' type expression what type it represents.
    */
  def typeExpressionType(expression: TypeExpression): Type =
    expression match {
      case NameTypeExpression(_, symbol) => symbol.`type`
      case ApplicationTypeExpression(constructor, arguments) =>
        ApplicationType(
          typeExpressionType(constructor),
          arguments.map(typeExpressionType)
        )
    }

  /**
    * Adds a type symbol to the scope for the given type definition, with the
    * `type` field of the symbol set to `null`.
    */
  def collectTypeNameAndKind(scope: Scope, definition: TypeDefinition): Unit = {
    val TypeDefinition(name, kindE, _) = definition
    checkKindExpression(scope, kindE)
    val kind = kindExpressionKind(kindE)
    val symbol = new TypeSymbol(kind, null)
    scope.addTypeSymbol(name, symbol)
  }

  /**
    * Verifies a type definition and annotates the type expression of it with
    * symbols.
    */
  def checkTypeDefinition(scope: Scope, definition: TypeDefinition): Unit = {
    val TypeDefinition(name, kindE, typeE) = definition
    val symbol = scope.getTypeSymbol(name)
    checkTypeExpression(scope, typeE)
    val `type` = typeExpressionType(typeE)
    val kind = typeKind(`type`)
    if (kind != symbol.kind) {
      throw KindMismatchError(symbol.kind, kind)
    }
    symbol.`type` = `type`
  }
}
