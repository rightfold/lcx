package lcx.check

import lcx.ast._
import lcx.check.scope.Scope
import lcx.types._

object kinds {
  /**
    * Verifies kind expressions and annotates them with symbols.
    */
  def checkKindExpression(scope: Scope, expression: KindExpression): Unit =
    expression match {
      case nke @ NameKindExpression(name, _) =>
        nke.symbol = scope.getKindSymbol(name)
      case FunctionKindExpression(from, to) =>
        from.foreach(k => checkKindExpression(scope, k._2))
        checkKindExpression(scope, to)
    }

  /**
    * Returns for any checked kind expression what kind it represents.
    */
  def kindExpressionKind(expression: KindExpression): Kind =
    expression match {
      case NameKindExpression(_, symbol) =>
        symbol.kind
      case FunctionKindExpression(from, to) =>
        FunctionKind(
          from.map(k => (k._1, kindExpressionKind(k._2))),
          kindExpressionKind(to)
        )
    }
}
