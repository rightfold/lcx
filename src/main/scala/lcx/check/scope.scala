package lcx.check

import lcx.check.error._
import lcx.symbols._
import scala.collection.mutable

object scope {
  class Scope(parent: Scope = null) {
    private val kinds = mutable.Map[String, KindSymbol]()
    private val types = mutable.Map[String, TypeSymbol]()
    private val values = mutable.Map[String, ValueSymbol]()

    def derive(): Scope = new Scope(this)

    def addKindSymbol(name: String, symbol: KindSymbol): Unit = {
      if (kinds contains name) {
        throw KindRedefinitionError(name)
      }
      kinds(name) = symbol
    }

    def getKindSymbol(name: String): KindSymbol =
      if (kinds contains name) {
        kinds(name)
      } else if (parent != null) {
        parent.getKindSymbol(name)
      } else {
        throw KindNotInScopeError(name)
      }

    def addTypeSymbol(name: String, symbol: TypeSymbol): Unit = {
      if (types contains name) {
        throw TypeRedefinitionError(name)
      }
      types(name) = symbol
    }

    def getTypeSymbol(name: String): TypeSymbol =
      if (types contains name) {
        types(name)
      } else if (parent != null) {
        parent.getTypeSymbol(name)
      } else {
        throw TypeNotInScopeError(name)
      }

    def addValueSymbol(name: String, symbol: ValueSymbol): Unit = {
      if (values contains name) {
        throw ValueRedefinitionError(name)
      }
      values(name) = symbol
    }

    def getValueSymbol(name: String): ValueSymbol =
      if (values contains name) {
        values(name)
      } else if (parent != null) {
        parent.getValueSymbol(name)
      } else {
        throw ValueNotInScopeError(name)
      }
  }
}
