package lcx.check

import lcx.types.{Kind, Type}

object error {
  case class KindRedefinitionError(name: String) extends Exception
  case class KindNotInScopeError(name: String) extends Exception

  case class TypeRedefinitionError(name: String) extends Exception
  case class TypeNotInScopeError(name: String) extends Exception

  case class ValueRedefinitionError(name: String) extends Exception
  case class ValueNotInScopeError(name: String) extends Exception

  case class KindMismatchError(expected: Kind, got: Kind) extends Exception
  case class TypeMismatchError(expected: Type, got: Type) extends Exception
}
