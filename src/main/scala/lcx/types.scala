package lcx

import lcx.symbols.ValueSymbol

object types {
  sealed abstract class Variance
  case object Invariant extends Variance
  case object Covariant extends Variance
  case object Contravariant extends Variance

  sealed abstract class Kind
  case object TypeKind extends Kind
  case class FunctionKind(from: List[(Variance, Kind)], to: Kind) extends Kind

  sealed abstract class Type
  case class FunctionType(parameterCount: Int) extends Type
  case class TupleType(elementsCount: Int) extends Type
  case class SingletonType(symbol: ValueSymbol) extends Type
  case class ApplicationType(constructor: Type, arguments: List[Type]) extends Type

  /**
    * Returns whether two types are the same type.
    */
  def equalType(a: Type, b: Type): Boolean =
    subtype(a, b) && supertype(a, b)

  /**
    * Returns whether `a` is a subtype of `b`.
    */
  def subtype(a: Type, b: Type): Boolean =
    (a, b) match {
      case (a, b) if a eq b => true
      case (FunctionType(apc), FunctionType(bpc)) => apc == bpc
      case (TupleType(an), TupleType(bn)) => an == bn
      case (SingletonType(as), SingletonType(bs)) => as == bs
      case (SingletonType(as), b) => subtype(as.`type`, b)
      case (ApplicationType(actor, aargs), ApplicationType(bctor, bargs)) =>
        subtype(actor, bctor) && {
          val FunctionKind(actorparams, _) = typeKind(actor)
          (actorparams.map(_._1), aargs, bargs).zipped.forall {
            case (Invariant,     aarg, barg) => equalType(aarg, barg)
            case (Covariant,     aarg, barg) => subtype(aarg, barg)
            case (Contravariant, aarg, barg) => supertype(aarg, barg)
          }
        }
      case _ => false
    }

  /**
    * Returns whether `b` is a subtype of `a`.
    */
  def supertype(a: Type, b: Type): Boolean =
    subtype(b, a)

  /**
    * Returns the kind of a type.
    */
  def typeKind(`type`: Type): Kind =
    `type` match {
      case FunctionType(parameterCount) =>
        FunctionKind(
          List.fill(parameterCount)((Contravariant, TypeKind)) :+ (Covariant, TypeKind),
          TypeKind
        )
      case TupleType(n) =>
        FunctionKind(List.fill(n)((Covariant, TypeKind)), TypeKind)
      case SingletonType(_) => TypeKind
      case ApplicationType(constructor, arguments) =>
        val FunctionKind(_, toKind) = typeKind(constructor)
        toKind
    }
}
