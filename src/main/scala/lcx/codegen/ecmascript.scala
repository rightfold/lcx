package lcx.codegen

import lcx.ast._

object ecmascript {
  object ast {
    sealed abstract class Node {
      def format: String
    }

    sealed abstract class Statement extends Node
    case class VariableStatement(name: String, value: Expression) extends Statement {
      override def format = s"var $name = ${value.format};"
    }

    sealed abstract class Expression extends Node
    case class NameExpression(name: String) extends Expression {
      override def format = name
    }
  }

  def definitionToStatements(definition: Definition): List[ast.Statement] =
    definition match {
      case _: TypeDefinition => Nil
      case ValueDefinition(name, _, value) =>
        val statement = ast.VariableStatement(name, expressionToExpression(value))
        List(statement)
    }

  def expressionToExpression(expression: ValueExpression): ast.Expression =
    expression match {
      case NameValueExpression(name, _) => ast.NameExpression(name)
    }
}
