package lcx.check

import lcx.ast._
import lcx.check.error.KindMismatchError
import lcx.check.scope.Scope
import lcx.check.types._
import lcx.symbols.TypeSymbol
import lcx.types._
import org.scalatest.FlatSpec

class typesSpec extends FlatSpec {
  "checkTypeExpression" should "succeed checking name type expressions" in {
    val symbol = new TypeSymbol(TypeKind, TupleType(2))
    val scope = new Scope()
    scope.addTypeSymbol("t", symbol)
    val expression = NameTypeExpression("t", null)
    checkTypeExpression(scope, expression)
    assert(expression.symbol eq symbol)
  }

  it should "succeed checking application type expressions" in {
    val scope = new Scope()

    val tSymbol = new TypeSymbol(TypeKind, ApplicationType(TupleType(0), Nil))
    scope.addTypeSymbol("t", tSymbol)

    val fSymbol = new TypeSymbol(
      FunctionKind(
        List((Contravariant, TypeKind), (Contravariant, TypeKind), (Covariant, TypeKind)),
        TypeKind
      ),
      FunctionType(2)
    )
    scope.addTypeSymbol("f", fSymbol)

    val expression = ApplicationTypeExpression(
      NameTypeExpression("f", null),
      List(NameTypeExpression("t", null), NameTypeExpression("t", null), NameTypeExpression("t", null))
    )
    checkTypeExpression(scope, expression)
    assert(expression.constructor.asInstanceOf[NameTypeExpression].symbol == fSymbol)
    assert(expression.arguments(0).asInstanceOf[NameTypeExpression].symbol == tSymbol)
    assert(expression.arguments(1).asInstanceOf[NameTypeExpression].symbol == tSymbol)
  }

  it should "fail checking application type expressions with kind mismatches" in {
    val scope = new Scope()

    val tSymbol = new TypeSymbol(TypeKind, ApplicationType(TupleType(0), Nil))
    scope.addTypeSymbol("t", tSymbol)

    val fSymbol = new TypeSymbol(
      FunctionKind(
        List((Contravariant, TypeKind), (Contravariant, TypeKind), (Covariant, TypeKind)),
        TypeKind
      ),
      FunctionType(2)
    )
    scope.addTypeSymbol("f", fSymbol)

    val expression = ApplicationTypeExpression(
      NameTypeExpression("f", null),
      List(NameTypeExpression("t", null), NameTypeExpression("f", null), NameTypeExpression("t", null))
    )
    intercept[KindMismatchError] {
      checkTypeExpression(scope, expression)
    }
  }
}
